<?php

namespace EdytaJordan\OpenWeather\Commands;

use Illuminate\Console\Command;

class ClearOpenWeatherData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:openweather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears Open Weather cached response from custom package.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //todo clear cache setup by Decorator
        echo "Open Weather response cache cleared in a package!!!";
    }
}
