<?php

namespace EdytaJordan\OpenWeather\Facades;

use Illuminate\Support\Facades\Facade;

class OpenWeather extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'openweather';
    }
}