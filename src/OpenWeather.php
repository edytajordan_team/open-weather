<?php


namespace EdytaJordan\OpenWeather;

use GuzzleHttp\Client;

class OpenWeather
{
    protected $apiKey;
    protected $endpoint         = 'https://api.openweathermap.org/data/2.5/';
    protected $endpoint_pro     = 'https://pro.openweathermap.org/data/2.5/';
    protected $params           = [];
    protected $client;
    protected $response         = [];

    /**
     * OpenWeather constructor
     */
    public function __construct()
    {
        $this->apiKey = config('openweather.apikey');
        $this->client = new Client();
    }

    /**
     * Builds the request and makes the call to the API based on passed options
     * Please see notes in Read.Me
     * You can get full response with this method (for partial response use helpers defined below)
     * Full response = what user passes in params by using include, exclude options
     *
     * $options = [
     * 'pro'                     => false,                   //if not provided, false is default
     * 'endpoint'                => 'weather',               //weather, forecast, box, group or onecall
     * 'custom_url'              => null,                    //if not provided, url will be built from other options
     * 'endpoint'                => 'weather',               //weather, forecast, box, group or onecall
     * 'location'                => 'zip',                   //zip, city name, city id, country code or lat and lon
     * 'location_value'          => $zipcode,
     * 'country_code'            => '',                      //if not provided, default is 'us'
     * 'include'                 => ['wind'],                //please refer to API documentation for examples of response params (fields)
     * 'exclude'                 => [],                      //please refer to API documentation for examples of response params (fields)
     * ];
     *
     *
     *
     * @param $params array
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWeather(array $params)
    {
        $this->params = $params;

        //build API URL based on provided options
        $url = $this->buildURL($params);

        //call HTTP client
        $response = $this->getResponse($url, $params);

        return $response;
    }

    /**
     * AS of August 2020 OpenWeather API has multiple endpoints depending on how you want to make the calls and what data to get
     * We can call buildURL method to pass any options to build endpoint(s) and query params OR just pass 1 custom_url
     *
     * //EXAMPLE $url = 'https://api.openweathermap.org/data/2.5/weather?zip=78251&&appid={your api key};
     *
     * @param $params array
     * @return mixed
     */
    public function buildURL(array $params)
    {
        //$this->params = $params;
        $url = $this->endpoint;

        //you can just pass your own custom url instead of using a builder
        if (!empty($params['custom_url'])) {
            $url = $params['custom_url'];
        } else {
            if ($params["pro"]) {
                $url = $this->endpoint_pro;
            }

            $url .= $params['type'];
            $url .= "?";

            if (!empty($params['lat'])) {
                $url .= 'lat=' . $params['lat'];
                $url .= '&lon=' . $params['lon'];
            } else {
                $url .= $params['location'] . "=";
                $url .= $params['location_value'];

                if (!empty($params['country_code'])) {
                    $url .= ',' . $params['country_code'];
                }
            }
        }

        $url .= '&appid=' . $this->apiKey;

        return $url;
    }

    /**
     * Get Response from API
     *
     * @param $url
     * @param $params
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getResponse($url, $params)
    {
        $response = [];

        try {
            $response = json_decode($this->client->get($url)->getBody(), true, 512, JSON_THROW_ON_ERROR);

            if (!empty($response)) {
                //check for included or excluded sections
                if (!empty($params['includeOnly'])) {
                    $response = $this->includeOnly($response, $params['includeOnly']);
                }
                if (!empty($params['excludeOnly'])) {
                    $response = $this->excludeOnly($response, $params['excludeOnly']);
                }
            }

            return $response;

        } catch (\JsonException $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * Include items from response (not the whole response)
     *
     * @param $response
     * @param array $params
     * @return array
     */
    public function includeOnly($response, $params = [])
    {
        $final_response = [];

        if (!empty($params)) {
            foreach ($params as $include) {
                $final_response[$include] = $response[$include];
            }
        }

        return $final_response;
    }

    /**
     * Exclude items from response (not the whole response)
     *
     * @param $response
     * @param array $params
     * @return mixed
     */
    public function excludeOnly($response, $params = [])
    {
        if (!empty($params)) {
            foreach ($params as $exclude) {
                unset($response[$exclude]);
            }
        }

        return $response;
    }

    //START other HELPERS
    /**
     * AS of August 2020 OpenWeather API has multiple endpoints depending on how you want to make the calls and what data to get
     *
     * Below helpers wil return different things
     *
     */
    //END Helpers
}