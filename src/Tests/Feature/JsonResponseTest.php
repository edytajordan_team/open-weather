<?php

namespace EdytaJordan\OpenWeather\Tests;

class JsonResponseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testCanSeeJsonResponse(){

        $response = $this->json('GET', 'api/v1/wind/78251');

        $response->assertStatus(200);
    }
}
