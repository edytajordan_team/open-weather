<?php

namespace EdytaJordan\OpenWeather;

use Illuminate\Support\ServiceProvider;

class OpenWeatherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $source = dirname(__DIR__) . '/config/openweather.php';

        $this->publishes([$source => config_path('openweather.php')]);

        $this->mergeConfigFrom($source, 'openweather');

        include __DIR__ . '/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // register controller
        $this->app->make('EdytaJordan\OpenWeather\OpenWeatherController');

        $this->app->bind('openweather', function () {

            return new OpenWeather();

        });
    }
}