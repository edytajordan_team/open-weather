<?php

namespace EdytaJordan\OpenWeather;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OpenWeatherController extends Controller
{
    /**
     * Use this controller for package specific methods if needed
     */
}