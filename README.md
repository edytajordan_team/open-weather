## Laravel OpenWeather

This is a Laravel style wrapper for the OpenWeather API.
For more information regarding request and response formats, visit: https://openweathermap.org/

### Install

Require this package with composer using the following command:

``` bash
$ composer require edytajordan/openweather
```


After updating composer, add the service provider to the `providers` array in `config/app.php`

```php
/*
* Package Service Providers...
*/
EdytaJordan\OpenWeather\OpenWeatherServiceProvider::class,
```

To register a facade accessor, add the following to `config/app.php` `aliases` array
```php
 "OpenWeather" => EdytaJordan\OpenWeather\Facades\OpenWeather::class,
```

### Configuration

Add the following line to the .env file:

```sh
OPEN_WEATHER_KEY=<your_open_weather_api_key>
```


### Usage
For full details of response formats, visit: https://openweathermap.org/api

##### Example Case: Get 'wind' information only for given zipcode. According to OpenWeather API final url passed should be:
``` php
$url = api.openweathermap.org/data/2.5/weather?zip={zip code},{country code}&appid={your api key}
```

##### Params
Pass an array of params for desired response. Below example will return WIND only from the weather information by requested zip code. URL Builder in OpenWeather class will build the url based on provided params. Also, it will include only the items requested or exclude items from the response. 

Optionaly, you can just pass 'custom_url' (final url) and the builder will skip building url and instead just validate and get a response. 
``` php
//example wih options to build URL
public function wind(Request $request, $zipcode)
    {
        //passing all the options as an example only, please refer to documentation
        $options = [
            'custom_url'        => null,        //if not provided, url will be built from other options
            'pro'               => false,       //if not provided, false is default
            'type'              => 'weather',   //weather, forecast, box, group or onecall (please refer to API documentation)
            'lat'               => null,
            'lon'               => null,
            'location'          => 'zip',       //zip, city name, city id
            'location_value'    => $zipcode,
            'country_code'      => '',          //if not provided, default is 'us'
            'includeOnly'       => ['wind'],    //RESPONSE WILL RETURN INCLUDED ITEMS ONLY (please refer to API documentation for examples of response params (fields) )
            'excludeOnly'       => [],          //RESPONSE WILL RETURN EXCLUDED ITEMS ONLY please refer to API documentation for examples of response params (fields)
        ];

       return json_encode(OpenWeather::getWeather($options));
    }
```
``` php
//example wih passing custom URL
public function wind(Request $request, $zipcode)
    {
        //passing all the options as an example only, please refer to documentation
        $options = [
            'custom_url'        => 'api.openweathermap.org/data/2.5/weather?zip='.$zipcode.'&appid={your api key}',       
            'includeOnly'       => ['wind'],    //RESPONSE WILL RETURN INCLUDED ITEMS ONLY (please refer to API documentation for examples of response params (fields) )
        ];

       return json_encode(OpenWeather::getWeather($options));
    }
```

#### JSON Response
``` json
{
  "wind": {
    "speed": 2.1,
    "deg": 0
  }
}
```

### License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
